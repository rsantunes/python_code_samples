# coding: utf-8

class FiboNotDefined(Exception):
   pass

class FiboTooHigh(Exception):
   pass

def fibo(n):
   if n > 30:
      raise FiboTooHigh(n)
   elif n < 1:
      raise FiboNotDefined(n)
   elif n < 3:
      return 1
   else:
      return fibo(n-1) + fibo(n-2)

def main():
   for it in range(-5,35):
      try:
         print(f"{it} -> {fibo(it)}")
      except FiboNotDefined as exc:
         print(f"Fibonacci sequence undefined for {exc.args[0]}.")
      except FiboTooHigh as exc:
         print(f"Calculating Fibonacci for {exc.args[0]} would take too long.")

if __name__ == "__main__":
   main()
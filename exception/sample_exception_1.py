# coding: utf-8

class FiboError(Exception):
   pass

def fibo(n):
   if n > 30:
      raise FiboError(f"Value {n} too high, execution time will be too long.")
   elif n < 1:
      raise FiboError(f"The Fibonacci sequence is not defined for {n}.")
   elif n < 3:
      return 1
   else:
      return fibo(n-1) + fibo(n-2)

def main():
   for it in range(-5,35):
      print(f"{it} -> {fibo(it)}")

if __name__ == "__main__":
   main()
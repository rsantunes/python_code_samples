# coding: utf-8

def main():
   try:
      arq = open("text_sample.txt", "r")
      for linha in arq:
         print(linha.strip())
   except IOError as exc:
      print(exc)
   finally:
      arq.close()
   print()

def main_with():
   with open("text_sample.txt", "r") as arq:
      for linha in arq:
         print(linha.strip())
   print()

if __name__ == "__main__":
   main()
   main_with()
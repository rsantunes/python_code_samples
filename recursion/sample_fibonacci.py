def fibo_rec(n):
    if n<3:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)

def fibo_iter(n):
    if n<3:
        return 1
    else:
        res = 1
        prev1 = 1
        prev2 = 1
        for _ in range(3, n+1):
            res = prev1 + prev2
            prev2 = prev1
            prev1 = res
        return res


print(fibo_iter(40))
print(fibo_rec(40))

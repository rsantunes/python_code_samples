def iter_fac(n):
    res = 1
    if n > 1:
        for it in range(2, n+1):
            res *= it
    return res

def rec_fac(n):
    if n < 2:
        return 1
    else:
        return n * rec_fac(n-1)

print(iter_fac(10))
print(rec_fac(10))
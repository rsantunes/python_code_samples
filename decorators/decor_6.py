class CallCounter:
	def __init__(self, func):
		self.func = func
		self.count = 0

	def __call__(self, *args, **kwargs):
		self.count += 1
		return self.func(*args, **kwargs)

	def GetCallCount(self):
		return self.count

@CallCounter
def fibo(n):
	if n < 3:
		return 1
	else:
		return fibo(n-1) + fibo(n-2)

for it in range(1,41):
	print(f"{fibo(it)} - {fibo.GetCallCount()}")

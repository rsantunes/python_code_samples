def aviso(mensagem):
	def decorator_aviso(func):
		def wrapper_aviso(*args, **kwargs):
			print(mensagem)
			return func(*args, **kwargs)
		return wrapper_aviso
	return decorator_aviso

@aviso("Working...")
def fact(n):
	if n < 2:
		return 1
	else:
		return n * fact(n-1)

print(f"O resultado é {fact(10)}")
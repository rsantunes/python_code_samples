def antes_depois(func):
	def wrapper(*args, **kwargs):
		print("Antes...")
		func(*args, **kwargs)
		print("Depois...")
	return wrapper

@antes_depois
def foo(valor):
	print(f"O valor é {valor}.")

foo(10)
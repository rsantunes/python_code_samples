def calcular(func):
	def wrapper(*args, **kwargs):
		print("Calculando...")
		return func(*args, **kwargs)
	return wrapper

@calcular
def fact(n):
	if n < 2:
		return 1
	else:
		return n * fact(n-1)

print(f"O resultado é {fact(10)}")
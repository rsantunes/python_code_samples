def antes_depois(func):
	def wrapper():
		print("Antes...")
		func()
		print("Depois...")
	return wrapper

@antes_depois
def foo():
	print("Olá!")

foo()
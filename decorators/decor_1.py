def antes_depois(func):
	def wrapper():
		print("Antes...")
		func()
		print("Depois...")
	return wrapper

def foo():
	print("Olá!")

bar = antes_depois(foo)

bar()
class singleton:
	def __init__(self, cls):
		self.instance = None
		self.cls = cls

	def __call__(self, *args, **kwargs):
		if not self.instance:
			self.instance = self.cls(*args, **kwargs)
		return self.instance

@singleton
class TheOne:
	pass

first_one = TheOne()
second_one = TheOne()

print(id(first_one))
print(id(second_one))

print(first_one is second_one)
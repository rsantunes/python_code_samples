from random import Random
from functools import reduce

def random_list(quant):
   rng = Random(1000)
   res = []
   for _ in range(quant):
      res.append(rng.random())
   return res

def print_list(data):
   for it in data:
      print(f"{it:.2f}", end=" ")
   print()

def main():
   rl = random_list(20)
   print_list( rl )

   #nl1 = list( map( lambda x: x**2, rl ) )
   nl1 = [x**2 for x in rl]
   print_list( nl1 )

   # nl2 = list( filter( lambda x: x > 0.3, nl1 ) )
   nl2 = [x > 0.3 for x in nl1]
   print_list( nl2 )

   # sum_nl1 = reduce(lambda x, y: x+y, nl1)
   # print(f"Somatório de nl1: {sum_nl1:.2f}")

   # sum_nl2 = reduce(lambda x, y: x+y, nl2)   
   # print(f"Somatório de nl2: {sum_nl2:.2f}")

def main_chain():
   rl = random_list(20)
   proc_list = reduce(lambda x, y: x+y, filter(lambda x: x > 0.3, map(lambda x: x**2, rl)))
   print(f"Somatório de nl1: {proc_list:.2f}")

if __name__ == "__main__":
   main()
   #main_chain()
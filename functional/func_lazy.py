
def main(n):
   fibo = lambda n: 1 if n < 3 else fibo(n-1) + fibo(n-2)

   fibo_list = list( map(fibo, range(1, n+1)) )

   for it in fibo_list:
      print(it)

if __name__ == "__main__":
   main(40)
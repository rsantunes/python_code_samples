import numpy as np
import random

def python_list(quant):
    data = np.empty(quant, dtype=np.float)
    for it in range(quant):
        data[it] = np.random.randint(1, 100001)
    print(np.sum(data))
    return

if __name__ == "__main__":
    quant = 10000000
    python_list(quant)
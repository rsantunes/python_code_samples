import random

def python_list(quant):
    data = []
    for _ in range(quant):
        data.append(random.randint(1, 100001))
    print(sum(data))
    return

if __name__ == "__main__":
    quant = 10000000
    python_list(quant)
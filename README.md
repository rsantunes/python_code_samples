# Python Code Samples

This repository contains very simple coding samples with the goal to demonstrate basic programming concepts in Python.

These samples require **Python 3**, with some of them requiring on **Python 3.6 and above**.
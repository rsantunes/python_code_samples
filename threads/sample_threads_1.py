# coding: utf-8

import threading

def fibo(n):
   if n < 3:
      return 1
   else:
      return fibo(n-1) + fibo(n-2)

def fibo_thread(n):
   print(f"{n} -> {fibo(n)}")

def main():
   tp = list()
   for _ in range(4):
      thread = threading.Thread( target=fibo_thread, args=(35,) )
      thread.start()
      tp.append(thread)
   for thread in tp:
      thread.join()

if __name__ == "__main__":
   main()
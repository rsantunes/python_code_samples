# coding: utf-8

from threading import Thread, Lock, Event
from random import Random
import time

class SharedBuffer():
   def __init__(self):
      self.data = list()
      self.mutex = Lock()
      self.hasdata = Event()

   def AddData(self, item):
      with self.mutex:
         print("Inserting item into buffer")
         self.data.append(item)
         if not self.hasdata.isSet():
            self.hasdata.set()

   def PopData(self):
      if not self.hasdata.isSet():
         self.hasdata.wait()
      with self.mutex:
         print("Removing item from buffer")
         item = self.data.pop(0)
         if len(self.data) < 1:
            self.hasdata.clear()
         return item


def Producer(buffer, quant):
   rng = Random()
   for _ in range(quant):
      buffer.AddData(rng.random())
      time.sleep(0.1)

def Consumer(buffer, quant):
   for _ in range(quant):
      item = buffer.PopData()
      print(f"The data is {item:.4f}")

def main():
   sb = SharedBuffer()
   prod = Thread( target=Producer, args=(sb, 30) )
   prod.start()
   cons = Thread( target=Consumer, args=(sb, 30) )
   cons.start()
   prod.join()
   cons.join()

if __name__ == "__main__":
   main()
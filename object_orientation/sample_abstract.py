from abc import ABC, abstractmethod

class Poligono(ABC):
    def __init__(self):
        super().__init__()
    
    @abstractmethod
    def area(self):
        pass

class Quadrado(Poligono):
    def __init__(self, lado):
        self.lado = lado
        super().__init__()
    
    def area(self):
        return self.lado ** 2

def main():
    #x = Quadrado(23)
    x = Poligono()
    print(x.area())

if __name__ == "__main__":
    main()
def main():
    std_str = 'aãeéuücç'
    print(std_str)
    byte_str = std_str.encode('cp850')
    print(byte_str)
    dec_str = byte_str.decode('cp850')
    print(dec_str)

if __name__ == "__main__":
    main()
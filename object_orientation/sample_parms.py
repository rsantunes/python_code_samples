def func_list(*parms, prt=False):
    if prt:
        for it in parms:
            print(it)

def func_dict(**parms):
    for it in parms:
        print(it, parms[it])

def main():
    func_list(1, 2, 3, 4, prt=True)
    func_dict(a=1, b=2, c=3, d=4)

if __name__ == "__main__":
    main()
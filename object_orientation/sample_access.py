class Teste:
    def __init__(self, valor):
        self.valor = valor
        self.__valor = valor
        #print(self.__valor)
        self.__teste()
        return
    
    def __teste(self):
        print("Eu sou oculto!")
        return
    
    def teste(self):
        print("Eu *não* sou oculto!")
        return

def main():
    t = Teste("teste")
    #print(t.__valor)
    #t.__teste()
    #t.teste()

if __name__ == "__main__":
    main()
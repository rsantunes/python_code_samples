class Conta:
    cofre = 0

    def __init__(self):
        self.valor = 0
    
    def deposito(self, valor):
        self.valor += valor
        Conta.cofre += valor
    
    def saque(self, valor):
        if self.valor >= valor:
            self.valor -= valor
            Conta.cofre -= valor
        else:
            print('ERRO: Saldo insuficiente')
    
    def saldo(self):
        return self.valor
    
    def balanco(self):
        return Conta.cofre
    


#if __name__ == "__main__":
#    main()
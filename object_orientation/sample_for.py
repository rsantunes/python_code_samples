def main():
    x = ['a', 'b', 'c', 'd', 'e']
    for it in x:
        print(it)

    print()

    for it in range(len(x)):
        print(x[it])

if __name__ == "__main__":
    main()
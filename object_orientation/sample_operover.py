class Conta:
    def __init__(self, inicial):
        self.valor = inicial
    
    def deposito(self, valor):
        self.valor += valor
    
    def saque(self, valor):
        if self.valor >= valor:
            self.valor -= valor
        else:
            print('ERRO: Saldo insuficiente')
    
    def __add__(self, o):
        return Conta(self.valor + o.valor)

    def saldo(self):
        return self.valor

#if __name__ == "__main__":
#    main()
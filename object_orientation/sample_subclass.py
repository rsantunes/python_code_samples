class Ponto2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def imprime(self):
        print(f"{self.x}, {self.y}")
        return

    def __str__(self):
        return f"{self.x}, {self.y}"


class Ponto3(Ponto2):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def imprime(self):
        print(f"{super().__str__()}, {self.z}")
        return

    def __str__(self):
        return f"{super().__str__()}, {self.z}"


def main():
    p1 = Ponto2(2, 3)
    p1.imprime()
    p2 = Ponto3(4, 5, 6)
    p2.imprime()
    return

if __name__ == "__main__":
    main()
class Conta:
    def __init__(self):
        self.valor = 0
    
    def deposito(self, valor):
        self.valor += valor
    
    def saque(self, valor):
        if self.valor >= valor:
            self.valor -= valor
        else:
            print('ERRO: Saldo insuficiente')
    
    def saldo(self):
        return self.valor

#if __name__ == "__main__":
#    main()
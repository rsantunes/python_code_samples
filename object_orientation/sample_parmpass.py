class Teste:
    def __init__(self, valor):
        self.valor = valor
        return

def doSomething(teste):
    print(teste)
    teste = 5
    print(teste)
    #print(id(teste), teste.valor)
    #teste.valor = 10
    #teste = Teste(10)
    #print(id(teste), teste.valor)
    return

def main():
    #a = Teste(5)
    a = 'foo'
    print(a)
    #print(id(a), a.valor)
    doSomething(a)
    print(a)
    #print(id(a), a.valor)

if __name__ == "__main__":
    main()